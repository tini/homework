'use strict';
var images;
var img;
function preload()
{
    img = loadImage ("himmel.jpg");
    images = {
        background: img
    }
}


function setup() {
    createCanvas(1000, 1000);
    img = new Background();
}

var xpos = 0;
var speed = 6;

function draw() {
    ellipse(xpos, 125, 250, 250);
    line(40 + xpos, 50, 40 + xpos, 100);
    line(-40 + xpos, 50, -40 + xpos, 100);
    fill(255, 220, 20);
    ellipse(xpos, 170, 50, 100);
    strokeWeight(5);
    ellipseMode(CENTER);
    xpos = xpos + speed;
    if ((xpos > width) || (xpos < 0)) {
        speed = speed * -1;
    }
}


function keyPressed() {
  console.log('key pressed!')
}

function mousePressed() {
  console.log('mouse pressed!')

}/*

function setup() {
    createCanvas(displayWidth, displayHeight);
    strokeWeight(10)
    stroke(0);
}

function touchMoved() {
    line(touchX, touchY, ptouchX, ptouchY);
    return false;
}*/